import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule} from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './page/page-not-found/page-not-found.component';
import { LoginComponent } from './login/login.component';

import { NewsService } from './service/news.service';
import { CategoryService } from './service/category.service';
import { AuthenticationService } from './security/authentication-service.service';
import { JwtUtilsService } from './security/jwt-utils.service';
import { TokenInterceptorService } from './security/token-interceptor.service';
import { CanActivateAuthGuard } from './security/can-activate-auth.guard';
import { MainComponent } from './page/main/main.component';
import { DetailsComponent } from './details/details.component';

const appRoutes: Routes = [
  //{ path: 'login', component: LoginComponent},
  { path: 'main', component: MainComponent, canActivate:[CanActivateAuthGuard]},
  {path: 'news/:id', component: DetailsComponent},
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  {path: '**', component: PageNotFoundComponent}
]


@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    LoginComponent,
    MainComponent,
    DetailsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes,
      {
        enableTracing: false
      }
    )
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    },
    AuthenticationService,
    CanActivateAuthGuard,
    JwtUtilsService,
    NewsService,
    CategoryService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
