import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NewsService } from '../service/news.service';
import { News } from '../model/news-model';
import { ActivatedRoute, Router } from '@angular/router'
@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DetailsComponent implements OnInit {
  news: News;
  constructor(private newsService: NewsService,
              private route: ActivatedRoute) {}

  ngOnInit()  {
    this.loadData();
  } 
  

  loadData() {
    this.route.params.subscribe(
      (params) => { 
        var id = +params['id'];
        this.newsService.getOne(id).subscribe(
          (resp: News) => {
            this.news = resp;
          });
      });
  }  

}
