import { Category } from './category-model';

export class News implements NewsInterface {
    public title: string;
    public category: Category;
    public description: string;
    public content: string;

    constructor(newsCfg: NewsInterface) {
        this.title = newsCfg.title;
        this.category = newsCfg.category;
        this.description = newsCfg.description;
        this.content = newsCfg.content;
    }
}

interface NewsInterface {
    title: string
    category: Category
    description: string;
    content: string;
}