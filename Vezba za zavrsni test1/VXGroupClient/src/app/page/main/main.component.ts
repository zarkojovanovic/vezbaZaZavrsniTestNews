import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { News } from '../../model/news-model';
import { Category } from '../../model/category-model';
import { NewsService } from '../../service/news.service';
import { CategoryService } from '../../service/category.service';
import { AuthenticationService } from '../../security/authentication-service.service';
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class MainComponent implements OnInit {

  newsToAdd: any={
    title:'',
    category: {
      name:''
    },
    description:'',
    content:''
  }
  categoryId: number;
  news: News[];
  categories: Category[];
  currentPage: number=0;
  totalPages: number;
  filterData={title:'', categoryId: ''}
  constructor(private newsService: NewsService,
              private categoryService: CategoryService,
              private authService: AuthenticationService) { }

  ngOnInit() {
    this.loadData();
    
  }

  loadData() {
    this.newsService.getNews(this.currentPage).subscribe(
      (resp) => {
        this.news = resp['content'];
        this.totalPages = resp['totalPages'];
      });
    this.categoryService.getCategories().subscribe(
      (resp) => {
      this.categories = resp['content'];
      }
    )
  }
  
  changePage(i:number){
    this.currentPage+=i;
    this.loadData();

  }
  filter(){
    this.newsService.getNews(0, this.filterData.title, this.filterData.categoryId).subscribe((resp)=>{
      this.news = resp['content'];
      this.totalPages = resp['totalPages'];
    })
  }
  isLoggedIn() {
    return this.authService.isLoggedIn();
  }
  isAdmin() {
    var currentUser = JSON.parse(localStorage.getItem('currentUser')); 
    if (currentUser === null) {
      return false;
    }
    if (currentUser.roles == 'ADMINISTRATOR') {
      return true;
    } else {
      return false;
    }
  }

  getTableClass(){
    if(this.isLoggedIn()===true){
      return "col-sm-8";
    }
    else{
      return "col-sm-12";
    }
  }
  save() {
    this.newsService.save(this.newsToAdd).subscribe(
      (resp) => {
        this.loadData();
        this.reset();
      }
    );
  }
  edit(news) {
    this.newsToAdd = news;
  }

  delete(id: number) {
    this.newsService.delete(id).subscribe(
      (resp) => {
          this.loadData();
      });
      
  } 
  
  reset(){
    this.newsToAdd={
      title:"",
     category: {
      name:""
      },
      description:"",
      content:""
    } 
  }
}

