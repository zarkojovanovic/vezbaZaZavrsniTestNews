import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';  
@Injectable()
export class CategoryService {
  private readonly path: string = 'api/categories';
  constructor(private httpClient: HttpClient) { }

  getCategories() {
    return this.httpClient.get(this.path)
      .catch((error: any) => 
        Observable.throw(error.message || 'Server error')
      );
  }
  getOne(id: number) {
    return this.httpClient.get(this.path+ "/" +id);
  }  
}
