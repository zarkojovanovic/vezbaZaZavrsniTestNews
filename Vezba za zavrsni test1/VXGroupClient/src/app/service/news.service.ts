import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { News } from '../model/news-model';
@Injectable()
export class NewsService {
  private readonly path = 'api/news';
  
  constructor(private httpClient: HttpClient) { }

  getNews(currentPage: number, title?: string, categoryId?: string) {
    let page = String(currentPage);
    let params: HttpParams = new HttpParams();

    if (title) {
      params = params.append('title', title);
    }
    
    if (+categoryId) {
      params = params.append('categoryId', categoryId);
    }
    return this.httpClient.get<News[]>(this.path, {params})
      .catch((error: any) =>
				Observable.throw(error.message || 'Server error')
			);
  }
  
  getOne(id:number){
    return this.httpClient.get(this.path+"/"+id);
  }
  save(newsToAdd: News) {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.httpClient.post(this.path, newsToAdd, { headers });
  }
  delete(id: number) {
    let headers = new HttpHeaders({ 'Content-Type': 'application/text' });
    return this.httpClient.delete(this.path+"/"+ id, { responseType: 'text', headers }).
        catch((error: any) =>
				Observable.throw(error.message || 'Server error')
      );

  }

}
