package vp.advancedjava.students.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import vp.advancedjava.students.model.Category;
import vp.advancedjava.students.service.CategoryService;

@RestController
public class CategoryController {
	
	@Autowired
	CategoryService categoryService;
	
	@GetMapping(value ="api/categories")
	public Page<Category> getAll(Pageable page) {
		final Page<Category> retVal = categoryService.findAll(page);
		return retVal;
	}
	
	@GetMapping(value = "api/categories/{id}")
	public Category getOne(@PathVariable Long id) {
		final Category retVal = categoryService.findOne(id);
		return retVal;
				
	}
}
