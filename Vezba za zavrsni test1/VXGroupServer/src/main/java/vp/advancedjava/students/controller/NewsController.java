package vp.advancedjava.students.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import vp.advancedjava.students.model.Category;
import vp.advancedjava.students.model.News;
import vp.advancedjava.students.service.CategoryService;
import vp.advancedjava.students.service.NewsService;



@RestController
public class NewsController {
	
	@Autowired
	NewsService newsService;
	
	@Autowired
	CategoryService categoryService;
	
	@GetMapping(value = "api/news")
	public Page<News> getNews(Pageable page, @RequestParam(required= false, defaultValue="") String title,
			@RequestParam(required=false, defaultValue="") String categoryId) {
		final Page<News> retVal;
		
		 if (!categoryId.isEmpty() && !title.isEmpty()) {
			 Category category = categoryService.findOne(Long.valueOf(categoryId));
	         retVal = newsService.findByTitleAndCategory(title, category, page);
	        } else if (!categoryId.isEmpty() && title.isEmpty()) {
	        	Category category = categoryService.findOne(Long.valueOf(categoryId));
	        	retVal = newsService.findByCategory(category, page);
	        } else if (categoryId.isEmpty() && !title.isEmpty()) {
	        	retVal = newsService.findByTitle(title, page);
	        } else {
	        	retVal = newsService.findAll(page);
	        }
		 
		 return retVal;
	}
	
	@GetMapping(value = "api/news/{id}")
	public News getOne(@PathVariable Long id) {
		final News retVal = newsService.findOne(id);
		
		return retVal;
	}
	
	@PostMapping(value = "api/news")
	public News post(@RequestBody News news) {
		
		final News retVal = newsService.save(news);
		
		return retVal;
	}
	@SuppressWarnings("rawtypes")
	@DeleteMapping(value = "api/news/{id}")
	public ResponseEntity remove(@PathVariable Long id) {
		final News news = newsService.findOne(id);
		if (news == null) {
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		} else {
		 newsService.remove(id);
		 return new ResponseEntity(HttpStatus.OK); }
	}
}
