package vp.advancedjava.students.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class News {

	@Id
	@GeneratedValue
	private Long id;
	
	private String title;
	
	@ManyToOne( fetch= FetchType.EAGER)
	private Category category;
	
	private String description;
	private String content;
	
	
	public News(Long id, String title, Category category, String description, String content) {
		super();
		this.id = id;
		this.title = title;
		this.category = category;
		this.description = description;
		this.content = content;
	}


	public News() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public Category getCategory() {
		return category;
	}


	public void setCategory(Category category) {
		this.category = category;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getContent() {
		return content;
	}


	public void setContent(String content) {
		this.content = content;
	}
	
	
	
	
}
