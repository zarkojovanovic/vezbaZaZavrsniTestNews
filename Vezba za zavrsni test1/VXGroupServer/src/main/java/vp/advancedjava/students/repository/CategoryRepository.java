package vp.advancedjava.students.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import vp.advancedjava.students.model.Category;

@Repository
public interface CategoryRepository extends JpaRepository <Category, Long> {

}
