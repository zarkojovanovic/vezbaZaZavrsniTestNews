package vp.advancedjava.students.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import vp.advancedjava.students.model.Category;
import vp.advancedjava.students.model.News;

@Repository
public interface NewsRepository extends JpaRepository <News, Long> {
	
	public Page<News> findByTitle(String title, Pageable page);
	
	public Page<News> findByCategory(Category category, Pageable page);
	
	public Page<News> findByTitleAndCategory(String title, Category category, Pageable page);
}
