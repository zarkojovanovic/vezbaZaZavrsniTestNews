package vp.advancedjava.students.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vp.advancedjava.students.model.Category;
import vp.advancedjava.students.repository.CategoryRepository;

@Service
public class CategoryService {
	
	@Autowired
	CategoryRepository categoryRepository;
	
	public Page<Category> findAll(Pageable page) {
		return categoryRepository.findAll(page);
	}

	public Category findOne(Long id) {
		return categoryRepository.findOne(id);
	}

	public Category save(Category category) {
		return categoryRepository.save(category);
	}

	public void remove(Long id) {
		categoryRepository.delete(id);
	}
}
