package vp.advancedjava.students.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vp.advancedjava.students.model.Category;
import vp.advancedjava.students.model.News;
import vp.advancedjava.students.repository.NewsRepository;

@Service
public class NewsService {

	@Autowired
	NewsRepository newsRepository;
	
	public Page<News> findAll(Pageable page) {
		return newsRepository.findAll(page);
	}

	public News findOne(Long id) {
		return newsRepository.findOne(id);
	}

	public News save(News news) {
		return newsRepository.save(news);
	}

	public void remove(Long id) {
		 newsRepository.delete(id);
	}
	
	public Page<News> findByTitle(String title, Pageable page) {
		return newsRepository.findByTitle(title, page);
	}
	
	public Page<News> findByCategory(Category category, Pageable page) {
		return newsRepository.findByCategory(category, page);
	}
	
	public Page<News> findByTitleAndCategory(String title, Category category, Pageable page) {
		return newsRepository.findByTitleAndCategory(title, category, page);
	}
}
